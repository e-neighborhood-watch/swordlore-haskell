module Moth (moth) where

import Basics
import Color
import Common
import Control.Monad
import Grub

mothPart :: IO String
mothPart = choose
  [ "wing"
  ]

mothTexture :: IO String
mothTexture = choose
  [ "silk"
  , "furry"
  , "wolly"
  , "fuzzy"
  ]

mothModifier :: IO String
mothModifier = chooseM
  [ sizeDescriptor
  , compassDirection
  , season
  , complexColor
  , owner
  ]

mothIdentifier :: IO String
mothIdentifier = chooseM
  [ return "cinnabar"
  , return "hawk"
  , liftM2 ((. ('-' :)) . (++)) simpleColor mothPart
  , royalAdj
  , worm
  ]

moth :: IO String
moth = (+++ "moth") <$> chooseM
  [ liftM2 (+++) mothModifier mothIdentifier 
  , mothIdentifier
  ]
