module Pokemon (pokemon) where

import Basics
import System.Random
import Control.Monad

compound :: IO String
compound = chooseM
  [ liftM2 (:) (choose "GPS")  $ liftM2 (:) (choose "oai") (choose $ pure <$> "rl")
  , liftM2 (:) (choose "TBFH") $ liftM2 (:) (choose "oa")  (choose $ pure <$> "xg")
  , liftM2 (:) (choose "DF") $ fmap ("ra" ++) (choose ["c", "g", "t", "p", "x"])
  , liftM2 (++) (choose ["Pr", "R"]) $ fmap ("o" ++) (choose ["b", "v", "t"])
  , choose
    [ "Ven"
    , "Bulb"
    , "Whirl"
    , "Scol"
    , "Cas"
    , "Spin"
    , "Met"
    , "Med"
    , "Mar"
    , "Mam"
    , "Lum"
    , "Sim"
    , "Wim"
    , "Lick"
    , "Iv"
    , "Gast"
    , "Amb"
    , "Fung"
    , "Blaz"
    ]
  ]
  
compoundSuffix :: IO String
compoundSuffix = chooseM
  [ return ""
  , return "a"
  , return "o"
  , return "i"
  , return "y"
  , liftM2 (:) (choose "io") $ liftM2 (:) (choose "sn") compoundSuffix
  ]

prefix :: IO String
prefix = chooseM
  [ chooseM
    [ choose
      [ "Veno"
      , "Veni"
      ]
    , return "Ivy"
    , return "Licky"
    , return "Vile"
    , return "Haxo"
    , return "Bulba"
    , return "Whirli"
    , return "Scoli"
    , return "Meta"
    , return "Wim"
    , return "Goliso"
    , return "Medi"
    , return "Sil"
    , return "Cas"
    , return "Zu"
    , return "Cro"
    , return "Woo"
    , return "Swoo"
    , return "Swa"
    , return "Noi"
    , return "Para"
    , return "Gene"
    , return "Spina"
    , return "Simi"
    , return "Pan"
    , return "Gor"
    , return "Toxi"
    , return "Pilo"
    , return "Mamo"
    , return "Sui"
    , return "Na"
    , return "Xa"
    , return "Zy"
    , return "Toge"
    , return "Gastro"
    , return "Lumino"
    , return "Ambi"
    , return "Maro"
    , return "Probo"
    , return "Garbo"
    , choose
      [ "Pory"
      , "Poly"
      ]
    ]
  , liftM2 (++) compound compoundSuffix
  ]

suffix :: IO String
suffix = chooseM
  [ return "pede"
  , return "moth"
  , return "tite"
  , return "tox"
  , return "tone"
  , return "tang"
  , return "mish"
  , return "lish"
  , return "luxe"
  , return "cune"
  , return "char"
  , return "vern"
  , return "bull"
  , return "sect"
  , return "sage"
  , return "sear"
  , return "saur"
  , return "swine"
  , return "pot"
  , return "pom"
  , return "pod"
  , return "xen"
  , return "pour"
  , return "pex"
  , return "plume"
  , return "plex"
  , return "ple"
  , return "s"
  , choose
    [ "dor"
    , "dore"
    ]
  , choose
    [ "gard"
    , "guard"
    , "garde"
    ]
  , choose
    [ "tu"
    , "too"
    ]
  , liftM2 (:) (choose "gc") $ chooseM 
    [ liftM2 (++) (choose ["r",""]) $ liftM2 (:) (choose "auo") (choose ["ss", "st", "nk"])
    , ("oo"++) <$> choose ["n", ""]
    ]
  , fmap ("ch"++) $ liftM2 (:) (choose "ia") $ choose
    [ "mp"
    , "m"
    , "n"
    , "me"
    , "ng"
    , "nder"
    , "k"
    , "c"
    , "rm"
    , ""
    ]
  , liftM2 (:) (choose "cgkn") $ (++ "n") <$> choose ["oo", "o", "e"]
  , liftM2 (:) (choose "pbdg") $ choose ["i", "y", "ie", "a"]
  , (:"ak") <$> choose "rw"
  , (:"at") <$> choose "nrb"
  ]

pokemon :: IO String
pokemon = liftM2 (++) prefix suffix
