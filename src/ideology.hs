import Prelude hiding (until)

import System.Random
import Control.Monad

until :: (Monad m) => (a -> Bool) -> m a -> m a
until condition action = do
  res <- action
  if condition res
    then return res
    else until condition action

choose :: [a] -> IO a
choose x = do
  index <- randomRIO (0, length x - 1)
  return $ x !! index

chooseM :: [IO a] -> IO a
chooseM = join . choose

linkWith :: String -> String -> String -> String
linkWith mid a b = a ++ mid ++ b

getUniques :: (Eq a) => IO a -> IO [a]
getUniques = go []
  where
    go [] f = do
      a <- f
      go [a] f
    go prevs f = chooseM [
        return prevs
      , do
        a <- f
        if elem a prevs
          then go prevs f
          else go (a : prevs) f
      ]

baddies :: IO String
baddies = choose [
    "Capitalist"
  , "Fascist"
  , "Consumerist"
  , "Corporatist"
  , "Imperialist"
  , "Revisionist"
  , "Authoritarian"
  , "War"
  , "Science"
  , "Civilization"
  , "Globalization"
  , "Nuclear"
  ]

genre :: IO String
genre = do
  let
    ppl = choose [
        "Mao" 
      , "Luxemburg"
      , "Stalin"
      , "Lenin"
      , "Hoxha"
      , "Trotsky"
      , "Possad"
      , "Marx"
      , "Tito"
      , "Kadar"
      , "Magon"
      , "Bordig"
      , "De Leon"
      , "Makhno"
      , "Bolshev"
      ]
    peoplism = chooseM [
        fmap (++ "ism") ppl
      , fmap (foldl1 (linkWith "-")) (getUniques $ fmap (++ "ism") ppl)
      ]
    idPrefix = choose [
        "Anarcho"
      , "Neo"
      , "Eco"
      , "Pan"
      ]
    isms = choose [
        "Left"
      , "Union"
      , "Commun"
      , "Anarch"
      , "Panarch"
      , "Veganarch"
      , "Social"
      , "Primitiv"
      , "Natur"
      , "Femin"
      , "Acceleration"
      , "Mutual"
      , "Syndical"
      , "Chart"
      , "Pascif"
      , "Lifestyl"
      , "Transhuman"
      , "Illegal"
      , "Ego"
      , "Nihil"
      , "Individual"
      , "Collectiv"
      , "Situation"
      , "Synthes"
      , "Third World"
      , "Naxal"
      , "Vanguard"
      , "Ludd"
      , "Structural"
      , "Postgender"
      , "Reform"
      , "Wobbly"
      , "Nonconform"
      ]
    broadIdeology = chooseM [
        fmap (++ "ism") isms
      , peoplism
      , liftM2 (linkWith "-") idPrefix $ chooseM [
          fmap (++ "ism") isms
        , peoplism
        ]
      ]
    prefix = choose [
        "Post"
      , "Pre"
      , "Anti"
      , "Crypto"
      , "Neo"
      ]
    religious = choose [
        "Christian"
      , "Islamic"
      , "Atheist"
      , "Jewish"
      , "Buddhist"
      , "Pagan"
      , "Occult"
      , "Secular"
      ]
    baseDescriptor = choose [
        "Queer"
      , "Intersectional"
      , "Classical"
      , "Contemporary"
      , "Utopian"
      , "Post-Scarcity"
      , "Green"
      , "Black"
      , "Democratic"
      , "Social"
      , "Left"
      , "Modern"
      , "Industrial"
      , "Council"
      , "Revolutionary"
      , "Insurrectionary"
      , "Libertarian"
      , "Green"
      , "Vegan"
      , "Radical"
      , "New"
      , "Environmental"
      , "Global"
      , "International"
      , "Orthodox"
      , "Postmodern"
      , "Scientific"
      , "DIY"
      , "Market"
      , "Progressive"
      , "Apolitical"
      , "Apoliticalhuihiuhiuhiuhuihiuhuihihihiuhi:q"
      ]
    descriptor = chooseM [
        fmap (++ "ist") isms
      , baseDescriptor
      , religious
      , do
          method <- choose [
               "Automated"
             , "Insurrectionary"
             ]
          gayness <- choose [
              " Queer"
            , " Gay"
            , " Pansexual"
            , ""
            ]
          spaceness <- choose [
              " Space"
            , ""
            ]
          return $ "Fully " ++ method ++ " Luxury" ++ gayness ++ spaceness
      , fmap ("Anti-" ++) baddies
      , liftM2 (linkWith "-") prefix $ chooseM [
          fmap (++ "ist") isms
        , baseDescriptor
        , religious
        ]
      ]
  chooseM [
       liftM2 (linkWith " ") descriptor broadIdeology
     , broadIdeology
     , fmap (++ " without adjectives") broadIdeology
     , liftM2 (++) (liftM2 (linkWith " ") descriptor broadIdeology) $ chooseM [
         fmap (\attr -> linkWith attr " with " " characteristics") $ chooseM [
             return "Chinese"
           , liftM2 (++) (chooseM [
               return ""
             , fmap (++ "-") idPrefix
             ]) $ fmap (++"ist") isms
           ]
       , return " in one country"
       ]
     , fmap ("Ironic " ++) genre
    ]
  
main :: IO ()
main = mapM_ (const $ genre >>= putStrLn) [1..5]

