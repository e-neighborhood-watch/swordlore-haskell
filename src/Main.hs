module Main where

import Types.Mushroom
import Types.Fruit

import Mushroom
import Fruit

import Universe

import Data.Set

import Control.Monad.Trans.State

import Lens.Simple

main :: IO ()
main = do
  -- shrooms <- evalStateT (sequence $ [1..20] >> [mushroom]) (Universe empty)
  fruits <- sequence $ [1..20] >> [newFruit]

  mapM_ (putStrLn . view fruitName) fruits
