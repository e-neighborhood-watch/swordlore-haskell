module Words (word, capitalize) where 

import Basics
import Control.Monad
import Control.Monad.Random
import Data.Char (toUpper)

capitalize :: String -> String
capitalize [] = []
capitalize (a : b) = toUpper a : b

fstMap :: (a -> b) -> (a, c) -> (b, c)
fstMap f (a, b) = (f a, b)

-- No y
vowel :: MonadRandom m => m String
vowel = choose ["a", "e", "i", "o", "u"]

diphthong :: MonadRandom m => m String
diphthong = choose ["ae", "ai", "au", "ee", "ea", "ie", "oo", "ou"]

leadingConsonant :: MonadRandom m => m String
leadingConsonant = do
  freeCon <- choose ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "qu", "r", "s", "t", "v", "w", "y", "z"]
  rCon <- choose ["b", "c", "d", "f", "g", "p", "t", "v", "w", "z"]
  lCon <- choose ["b", "c", "f", "g", "p", "s"]
  choose [freeCon, rCon ++ "r", lCon ++ "l"]


cleanUp :: String -> String
cleanUp ('q' : 'u' : 'u' : x) = cleanUp $ "quo" ++ x
cleanUp (a : b) = a : cleanUp b
cleanUp [] = []

diphthongEndingConsonant :: MonadRandom m => m String
diphthongEndingConsonant = choose ["b", "d", "f", "g", "ck", "l", "m", "n", "p", "r", "s", "t", "v", "x", "z"]
monophthongEndingConsonant :: MonadRandom m => m String
monophthongEndingConsonant = choose ["b", "d", "f", "g", "ck", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "y", "z"]

openSyllables :: MonadRandom m => m [String]
openSyllables = do
  con1 <- leadingConsonant
  vow  <- vowel
  con2 <- monophthongEndingConsonant
  let cuc = con1 ++ vow ++ con2 

  con1 <- leadingConsonant
  diph <- diphthong
  con2 <- diphthongEndingConsonant
  let cuuc = con1 ++ diph ++ con2

  cu <- liftM2 (++) leadingConsonant vowel

  uc <- liftM2 (++) vowel monophthongEndingConsonant

  return [cuc, cuuc, cu, uc]

rightClosedSyllable :: MonadRandom m => m String
rightClosedSyllable = liftM2 (++) leadingConsonant diphthong

leftClosedSyllable :: MonadRandom m => m String
leftClosedSyllable = liftM2 (++) diphthong diphthongEndingConsonant

getSyllable :: MonadRandom m => Bool -> m (String, Bool)
getSyllable True = do
  os <- openSyllables
  lcs <- leftClosedSyllable
  choose $ map (flip (,) False) os ++ [(lcs, True)]
getSyllable False = do
  os <- openSyllables
  rcs <- rightClosedSyllable
  lcs <- leftClosedSyllable
  choose $ map (flip (,) False) os ++ [(rcs, False), (lcs, True)]

addSyllable :: MonadRandom m => (String, Bool) -> m (String, Bool)
addSyllable (lastWord, closed) = fmap (fstMap (++ lastWord)) $ getSyllable closed

subWord :: MonadRandom m => m (String, Bool)
subWord = id =<< choose [getSyllable False, addSyllable =<< subWord]

word :: MonadRandom m => m String
word = fmap (cleanUp . fst) subWord
  
