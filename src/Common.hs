module Common where

import Basics
import Words

import Control.Monad.Random

owner ::
  ( MonadRandom m
  )
    => m String
owner = ((++ "'s") . capitalize) <$> word

sizeDescriptor ::
  ( MonadRandom m
  )
    => m String
sizeDescriptor = choose
  [ "giant"
  , "dwarf"
  , "great"
  , "king"
  , "queen"
  ]

compassDirection ::
  ( MonadRandom m
  )
    => m String
compassDirection = choose 
  [ "northern"
  , "eastern"
  , "southern"
  , "western"
  ]

season ::
  ( MonadRandom m
  )
    => m String
season = choose
  [ "winter"
  , "autumn"
  , "summer"
  , "spring"
  ]

royalAdj ::
  ( MonadRandom m
  )
    => m String
royalAdj = choose
  [ "king"
  , "queen"
  , "imperial"
  , "royal"
  , "monarch"
  , "admiral"
  , "regal"
  ]
