module Flower where

import Basics
import System.Random
import Control.Monad

(+++) = (++) . (++ " ")

flowerPart :: IO String
flowerPart = choose
  [ "petal"
  , "leaf"
  , "stem"
  , "root"
  ]

partDescriptor :: IO String
partDescriptor = choose
  [ "waxy"
  , "small"
  ]

flowerColor :: IO String
flowerColor = choose
  [ "black"
  , "white"
  , "red"
  , "pink"
  , "purple"
  ]

flowerRelatedNoun :: IO String
flowerRelatedNoun = choose
  [ "frog"
  , "lady"
  , "angel"
  , "butterfly"
  , "ghost"
  , "crab"
  , "spider"
  , "tiger"
  , "helmet"
  ]

flowerPattern :: IO String
flowerPattern = choose
  [ "spotted"
  , "striped"
  ]

flowerDescriptor :: IO String
flowerDescriptor = chooseM
  [ flowerPattern
  , flowerRelatedNoun
  , liftM2 (+++) partDescriptor flowerPart
  , liftM2 (+++) flowerColor flowerRelatedNoun
  ]

orchid :: IO String
orchid = liftM2 (+++) flowerDescriptor (choose ["orchid", "pogonia"])
