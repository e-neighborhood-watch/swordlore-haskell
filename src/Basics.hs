module Basics 
  ( choose
  , chooseM
  , branch
  , (+++)
  ) where

import Control.Monad
import Control.Monad.Random

-- Maybe make this less biased towards earlier entries?
choose ::
  ( MonadRandom m
  )
    => [a] -> m a
choose l = (l !!) <$> getRandomR (0, length l - 1)

chooseM :: 
  ( MonadRandom m
  )
    => [m a] -> m a
chooseM = join . choose

branch ::
  ( MonadRandom m
  )
    => m a -> m a -> m a
branch m1 m2 = do
  a <- m1
  b <- m2
  choose [a, b]

(+++) :: String -> String -> String
(+++) = (++) . (++ " ")
