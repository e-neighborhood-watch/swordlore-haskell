{-# Language FlexibleContexts #-}
module Mushroom where

import Basics
import Color
import Wood
import Words
import Common
import Universe

import Types.Mushroom
import Types.Wood

import Control.Monad
import Control.Monad.Random
import Control.Monad.State

import Lens.Simple

shroomTexture ::
  ( MonadRandom m
  )
    => m String
shroomTexture = choose
  [ "velvet"
  , "felt"
  , "hairy"
  , "woolly"
  , "shaggy"
  , "scaly"
  , "waxy"
  , "rubbery"
  , "fuzy"
  , "felt"
  , "fleshy"
  , "jelly"
  , "sticky"
  , "viscid"
  ]

shroomPartAdjective ::
  ( MonadRandom m
  )
    => m String
shroomPartAdjective = chooseM
  [ return "worm"
  , return "wax"
  , return "web"
  , return "death"
  , return "skull"
  , return "wood"
  , return "funnel"
  , return "milk"
  , return "wine"
  , return "sap"
  , return "honey"
  , return "ink"
  , return "stink"
  , return "fiber"
  , return "jelly"
  , return "plump"
  , return "round"
  , return "big"
  , return "crown"
  , return "elf"
  , simpleColor
  , shroomTexture
  ]

shroomPart ::
  ( MonadRandom m
  )
    => m String
shroomPart = choose
  [ "cap"
  , "cup"
  , " helmet"
  , "hood"
  , "horn"
  ]

fruitPeel ::
  ( MonadRandom m
  )
    => m String
fruitPeel = liftM2 (+++) (choose
  [ "orange"
  , "banana"
  , "lemon"
  , "lime"
  ]) $ choose
    [ "peel"
    , "rind"
    ]

genericShroomAdjective ::
  ( MonadRandom m
  )
    => m String
genericShroomAdjective = chooseM
  [ return "clouded"
  , return "splendid"
  , return "fluted"
  , return "umbrella"
  , return "sweet"
  , return "deadly"
  , return "dazzling"
  , return "true"
  , return "false"
  , return "blushing"
  , return "shady"
  , return "common"
  , return "doughnut"
  ]

tongueShroom ::
  ( MonadRandom m
  )
    => m String
tongueShroom = do
  (+++ "tongue") <$> chooseM
    [ return "cat's"
    , return "earth"
    , shroomTexture
    ]

simpShroomType ::
  ( MonadRandom m
  )
    => m String
simpShroomType = choose
  [ "puffball"
  , "mushroom"
  , "toadstool"
  , "morel"
  , "fungus"
  , "bolete"
  ]

angelType ::
  ( MonadRandom m
  )
    => m String
angelType = chooseM 
  [ return "death"
  , return "destroying"
  , return "black"
  , return "weeping"
  , return "crying"
  , return "bleeding"
  , return "dying"
  , simpleColor
  ]

lightCreature ::
  ( MonadRandom m
  )
    => m String
lightCreature = choose
  [ "angel"
  , "fairy" 
  , "maiden"
  ]

cauldron ::
  ( MonadRandom m
  )
    => m String
cauldron = choose
  [ "cauldron"
  , "kettle"
  , "pot"
  ]

shroomGrowthPlace ::
  ( MonadRandom m
  )
    => m String
shroomGrowthPlace = choose 
  [ "meadow"
  , "hill"
  , "forest"
  , "swamp"
  , "bog"
  , "field"
  , "snowbank"
  , "stump"
  , "dune"
  , "steppe"
  , "mountain"
  , "shade"
  ]

saddleShroom ::
  ( MonadRandom m
  )
    => m String
saddleShroom = do
  let
    saddleOwner = choose
      [ "dryad's"
      , "fairy's"
      , "nymph's"
      , "witch's"
      , "maiden's"
      , "elven"
      ]
  (+++ "saddle") <$> chooseM
    [ saddleOwner
    , complexShroomAdjective 
    , liftM2 (+++) complexShroomAdjective saddleOwner
    ]

antlerShroom ::
  ( MonadRandom m
  )
    => m String
antlerShroom = do
  animal <- choose
    [ "buck"
    , "elk"
    , "moose"
    , "deer"
    , "stag"
    , "reindeer"
    , "caribou"
    ]
  antlerAdjective <- chooseM
    [ choose
      [ "carbon"
      , "rubber"
      ]
    , complexColor
    , shroomTexture
    ]

  (+++ "antler") <$> choose
    [ animal
    , antlerAdjective
    ]

spread ::
  ( MonadRandom m
  )
    => m String
spread = choose
  [ "butter"
  , "jelly"
  , "jam"
  ]

-- Sort of experimental this makes names that are a little less traditional
weirdShroom ::
  ( MonadRandom m
  )
    => m String
weirdShroom = do
  earType <- chooseM
    [ return "wood"
    , return "tree"
    , return "pig's"
    , return "bear's"
    , return "hare's"
    , return "rabbit's"
    , return "monkey's"
    , complexColor
    ]

  shroomType <- chooseM 
    [ simpShroomType
    , liftM2 (++) shroomPartAdjective shroomPart
    ]

  nickName <- chooseM
    [ choose
      [ "turkey"
      , "chicken"
      , "hen"
      , "partige"
      , "pheasant"
      , "duck"
      ]
    , chooseM
      [ simpShroomType
      , liftM2 (++) shroomPartAdjective shroomPart
      ]
    ]

  chooseM
    [ ((nickName +++ "of the") +++) <$> shroomGrowthPlace
    , chooseM
      [ liftM2 (+++) angelType lightCreature
      , liftM2 (+++) owner $ liftM2 (+++) angelType lightCreature
      ]
    , return $ earType +++ "ear"
    , flip (liftM2 (+++)) (branch cauldron spread) $ chooseM
      [ return "witch's"
      , return "witches'"
      , return "devil's"
      , owner
      ]
    , saddleShroom
    , antlerShroom
    , liftM2 (+++) complexShroomAdjective tongueShroom
    ]

complexShroomAdjective ::
  ( MonadRandom m
  )
    => m String
complexShroomAdjective = do
  descriptionPart <- choose ["capped", "cupped", "hoodded", "ringed", "stalked", "bulbed"]
  descriptionPartAdjective <- chooseM
   [ complexColor
   , shroomTexture
   ]

  chooseM
    [ complexColor
    , genericShroomAdjective
    , season
    , compassDirection
    , sizeDescriptor
    , shroomGrowthPlace
    , view woodName <$> basicWood
    , owner
    , antlerShroom
    , fruitPeel
    , shroomPartAdjective
    , shroomTexture
    , (+++ "jelly") <$> simpleColor
    , return $ descriptionPartAdjective ++ "-" ++ descriptionPart
    ]

newMushroom ::
  ( MonadRandom m
  , MonadState u m
  )
    => m Mushroom
newMushroom = do
  shroomType <- chooseM 
    [ simpShroomType
    , liftM2 (++) shroomPartAdjective shroomPart
    ]

  name <- chooseM
    [ (+++ shroomType) <$> complexShroomAdjective
--    , weirdShroom
    ]

  return Mushroom
    { _mushroomName      = name
    , _mushroomPoisonous = False
    }

addNewMushroom ::
  ( MonadRandom m
  , MonadState u m
  , ContainerOf u Mushroom
  )
    => m Mushroom
addNewMushroom = do
  m <- newMushroom
  addNew m
  return m

mushroom ::
  ( MonadRandom m
  , MonadState u m
  , ContainerOf u Mushroom
  )
    => m Mushroom
mushroom = chooseM
  [ do
    val <- generateFromOld
    case val of
      Nothing -> addNewMushroom
      Just shroom -> return shroom
  , addNewMushroom
  ]
