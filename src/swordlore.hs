import Basics
import Words
import Control.Monad

(##) :: String -> String -> String
(##) = (. (' ' :)) . (++)

tuple :: (Num a, Eq a) => a -> Maybe String
tuple 1 = Just "single"
tuple 2 = Just "double"
tuple 3 = Just "triple"
tuple 4 = Just "quadruple"
tuple _ = Nothing

vesselFlute :: IO String
vesselFlute = do
  chambers <- choose ["double", "triple", "traverse", "inline"]
  fluteType <- choose ["xun", "ocarina", "hun", "gemshorn", "molinukai", "borrindos"]
  choose [fluteType, chambers ## fluteType, fluteType ## "pendant"]

metal :: IO String
metal = do
  metalColor <- simpleColor
  metalModifier <- choose ["rose", "wood", metalColor]
  metalType <- choose ["metal", "steel", "gold"]
  basicMetal <- choose ["silver", "gold", "steel", "iron", "brass", "tin", "bronze", "aluminium", "lead", "copper", "nickel", "pewter"]

  choose [basicMetal, metalModifier ## metalType]

-- Still unfinished
cetacean :: IO String
cetacean = do
   family <- choose ["whale", "dolphin", "porpoise"]
   return $ family

camelini :: IO String
camelini = do
   choose ["dromedary", "camel", "camelops"]

lamini :: IO String
lamini = do
  llamaType <- choose ["ccara", "tapada", "lanuda"]
  choose ["llama", "alpaca", "vicuna", "guacano", "huacaya", llamaType ## "llama"]

sheep :: IO String
sheep = do
  habitat <- choose ["mountain", "alpine"]
  furColor <- choose ["red", "black"]
  hornAdjective <- fmap (++"horned") $ choose ["big", "thin", ""]
  rareFamily <- choose ["latxa", "xalda"]
  family <- choose ["argali", "mouflon", "sheep", "churra", "merino", rareFamily]
  choose [family, habitat ## family, furColor ## family, hornAdjective ## family]

goat :: IO String
goat = do
  habitat <- choose ["mountain", "alpine"]
  furColor <- choose ["red", "black"]
  hornAdjective <- fmap (++"horned") $ choose ["big", "thin", ""]
  rareFamily <- choose ["landrace", "markhor"]
  family <- choose ["goat", rareFamily]
  choose [family, habitat ## family, furColor ## family, hornAdjective ## family]

wool :: IO String
wool = do
   sheepSource <- sheep
   laminiSource <- lamini
   woolSource <- choose [sheepSource, laminiSource]
   fibreName <- choose ["qiviut", "cashmere", "mohair"]
   choose [woolSource ## "wool", fibreName]

evilEpithet :: IO String
evilEpithet = do
  destructionMethod <- choose ["destroyer", "devourer", "eater"]
  destructionTarget <- choose ["worlds", "souls"]

  destructionTarget' <- choose ["world", "soul"]

  title <- choose ["lord", "god", "duke", "prince"]

  domain <- choose ["blood", "worms", "snakes", "bones", "death", "the dead", "the damned", "the wretched", "the wastes"]

  keepee <- choose ["the worms", "the snakes", "souls", "the hounds"]

  magnitude <- choose ["hundred", "thousand", "million"]

  descriptor <- choose ["impossible", "endless", "timeless", "unending", "unseen", "unseeing", "undying", magnitude ++ "-eyed"]

  horrorType <- choose ["horror", "god"]

  location <- branch (return "") $ fmap (" at"##) $ choose ["the center of creation", "the edge of the world"]

  choose [
    destructionMethod ## "of" ## destructionTarget,
    destructionTarget' ## destructionMethod,
    title ## "of" ## domain,
    "keeper of" ## keepee,
    "the" ## descriptor ## horrorType ++ location]
