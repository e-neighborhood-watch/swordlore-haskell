{-# Language TemplateHaskell, MultiParamTypeClasses, FlexibleInstances #-}
module Universe where

import Prelude hiding (null)

import Basics

import Types.Mushroom

import Control.Monad.Random
import Control.Monad.State

import Data.Set

import Lens.Simple

data Universe = Universe
  { _mushrooms :: Set Mushroom
  }
makeLenses ''Universe

class Ord b => ContainerOf a b where
  -- This is equivalent to
  -- Functor f => LensLike' f a (Set b)
  -- but LensLike' is not present in Lens.Simple
  members :: Functor f => LensLike f a a (Set b) (Set b)

addNew ::
  ( ContainerOf a b
  , MonadState a m
  )
    => b -> m ()
addNew b = get >>= put . over members (insert b)

generateFromOld ::
  ( MonadRandom m
  , MonadState a m
  , ContainerOf a b
  ) 
    => m (Maybe b)
generateFromOld = do
  set <- view members <$> get
  if null set
    then return Nothing
    else Just <$> choose (toList set)

instance ContainerOf Universe Mushroom where
  members = mushrooms
