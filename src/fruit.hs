module Fruit where

import Basics
import Common
import Words
import Types.Fruit

import Control.Monad
import Control.Monad.Random
import Lens.Simple

fruitFlavor ::
 ( MonadRandom m
 )
   => m String
fruitFlavor = choose
  [ "sweet"
  , "sour"
  , "bitter"
  , "tangy"
  ]

biome :: 
  ( MonadRandom m
  )
    => m String
biome = choose
  [ "desert"
  , "mountain"
  , "key"
  , "cape"
  , "steppe"
  , "jungle"
  ]

firmness ::
  ( MonadRandom m
  )
    => m String
firmness = choose
  [ "hard"
  , "firm"
  , "soft"
  ]

texture ::
  ( MonadRandom m
  )
    => m String
texture = choose
  [ "rough"
  , "smooth"
  , "waxy"
  ]

fruitAdj ::
  ( MonadRandom m
  )
    => m String
fruitAdj = chooseM
  [ royalAdj
  , fruitFlavor
  , season
  , biome
  , firmness
  , texture
  , compassDirection
  , sizeDescriptor
  , royalAdj
  , return "round"
  , return "wild"
  , return "blood"
  , return "garden"
  ]

basicFruit ::
  ( MonadRandom m
  )
    => m Fruit
basicFruit = Fruit <$> choose
  [ "apple"
  , "apricot"
  , "banana"
  , "cherry"
  , "fig"
  , "kiwi"
  , "lemon"
  , "lime"
  , "mango"
  , "orange"
  , "papaya"
  , "peach"
  , "pear"
  , "pineapple"
  , "plum"
  ]

basicCitrus ::
  ( MonadRandom m
  )
    => m Fruit
basicCitrus = Fruit <$> choose
  [ "citron"
  , "etrog"
  , "lemon"
  , "lime"
  , "limetta"
  , "lumia"
  , "kumquat"
  , "pomelo"
  , "orange"
  , "kinnow"
  , "grapefruit"
  , "tangor"
  ]

portmanteauCitrus ::
  ( MonadRandom m
  )
    => m Fruit
portmanteauCitrus = do
  prefix <- choose
    [ "tang"
    , "orang"
    , "citrang"
    , "pomm"
    , "clem"
    , "lim"
    , "nect"
    ]
  suffix <- choose
    [ "erine"
    , "elo"
    , "entine"
    , "etta"
    , "equat"
    ]
  return $ Fruit $ prefix ++ suffix

citrus :: 
  ( MonadRandom m
  )
    => m Fruit
citrus = liftM2 (over fruitName . (+++)) fruitAdj $ chooseM
  [ basicCitrus
  , portmanteauCitrus
  ]

fruitWord ::
  ( MonadRandom m
  )
    => m String
fruitWord = chooseM
  [ return "passion"
  , return "star"
  , return "boysen"
  , return "crab"
  , return "fox"
  , return "honey"
  , return "wine"
  , return "wolf"
  , return "woad"
  , return "ghost"
  , choose
    [ "rawst"
    , "chesto"
    , "oran"
    , "aspear"
    , "pecha"
    , "leppa"
    , "wiki"
    ]
  , word
  ]

basicBerry :: 
  ( MonadRandom m
  )
    => m Fruit
basicBerry = over fruitName (++ "berry") <$> chooseM
  [ Fruit <$> choose
    [ "elder"
    , "rasp"
    , "straw"
    , "bar"
    , "bil"
    , "mul"
    , "choke"
    , "goose"
    , "cloud"
    , "huckle"
    , "snow"
    , "cran"
    , "crow"
    , "dew"
    , "lingon"
    , "logan"
    , "thimble"
    ]
  , Fruit <$> fruitWord
  , basicFruit
  ]

newBerry :: 
 ( MonadRandom m
 )
   => m Fruit
newBerry = liftM2 (over fruitName . (+++)) fruitAdj basicBerry

newFruit ::
 ( MonadRandom m
 )
   => m Fruit
newFruit = chooseM
  [ citrus
  , newBerry
  , Fruit <$> chooseM
    [ liftM2 (+++) fruitAdj $ (++ "fruit") <$> fruitWord
    , (++ "fruit") <$> fruitWord
    ]
  ]
