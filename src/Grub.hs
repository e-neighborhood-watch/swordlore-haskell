module Grub (worm) where

import Basics
import Types.Wood
import Wood

import Control.Monad.Random
import Lens.Simple

type Worm = String

worm ::
  ( MonadRandom m
  )
    => m Worm
worm = (++ "worm") <$> chooseM
  [ choose
    [ "canker"
    , "silk"
    , "blood"
    , "woad"
    , "span"
    , "meal"
    , "inch"
    , "bag"
    , "ear"
    , "horn"
    , "web"
    , "wax"
    , "tube"
    , "blind"
    , "glow"
    , "book"
    , "muck"
    , "ship"
    , "ring"
    , "flat"
    , "whip"
    , "hair"
    , "fire"
    , "earth"
    , "water"
    , "lung"
    , "wood"
    , "leaf"
    ]
  , view woodName <$> basicWood
  ]
