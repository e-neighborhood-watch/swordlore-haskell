{-# Language FlexibleContexts #-}
module Wood where

import Types.Wood
import Universe
import Basics
import Common
import Fruit

import Control.Monad
import Control.Monad.Random
import Control.Monad.State
import Lens.Simple

basicWood ::
 ( MonadRandom m
 )
  => m Wood
basicWood = Wood <$> choose 
  [ "poplar"
  , "pine"
  , "oak"
  , "spruce"
  , "birch"
  , "cedar"
  , "fir"
  , "yew"
  , "larch"
  , "alder"
  , "ash"
  , "aspen"
  , "elm"
  , "sycamore"
  , "mahogany"
  , "willow"
  , "maple"
  ]

woodColor ::
 ( MonadRandom m
 )
   => m String
woodColor = choose
  [ "red"
  , "white"
  , "black"
  , "silver"
  ]

newWood ::
  ( MonadRandom m
  )
    => m Wood
newWood = do
  woodColor <- woodColor
  woodAdj <- choose ["iron", "stone", "dog", "cotton", "cork", "musk", "shag", "rough", "king", "lace", woodColor]
  woodOrBark <- choose ["wood", "bark"]
  simpWoodType <- view woodName <$> basicWood
  woodType <- choose [woodAdj ++ woodOrBark, simpWoodType]
  habDirection <- compassDirection
  habbitat <- choose ["mountain", "alpine", "desert", "canyon", "swamp"]
  genericAdjective <- choose ["sweet", "rock", "sugar", "soft"]
  Wood <$> choose
    [ woodType
    , woodAdj ++ "bark" +++ simpWoodType
    , habDirection +++ woodColor +++ woodType
    , woodColor +++ woodType
    , habDirection +++ woodType
    , genericAdjective +++ woodType
    , habbitat +++ woodType
    ]

addNewWood ::
  ( MonadRandom m
  , MonadState u m
  , ContainerOf u Wood
  )
    => m Wood
addNewWood = do
  w <- newWood
  addNew w
  return w

wood ::
  ( MonadRandom m
  , MonadState u m
  , ContainerOf u Wood
  )
    => m Wood
wood = chooseM
  [ do
    val <- generateFromOld
    case val of
      Nothing -> addNewWood
      Just wood -> return wood
  , addNewWood
  ]
