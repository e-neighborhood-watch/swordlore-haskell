import Pokemon
import Pokemove

import Control.Monad

foo :: IO String
foo = do
  pkmn <- pokemon
  move <- pokemove

  return $ pkmn ++ " used " ++ move ++ "!"
