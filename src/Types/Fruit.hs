{-# Language TemplateHaskell #-}
module Types.Fruit where

import Lens.Simple

data Fruit = Fruit
  { _fruitName :: String
  }
  deriving
    ( Eq
    , Ord
    )
makeLenses ''Fruit
