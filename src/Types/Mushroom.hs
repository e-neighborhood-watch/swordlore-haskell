{-# Language TemplateHaskell #-}
module Types.Mushroom where

import Lens.Simple

data Mushroom = Mushroom
  { _mushroomName      :: String
  , _mushroomPoisonous :: Bool
  }
  deriving
    ( Ord
    , Eq
    )
makeLenses ''Mushroom
