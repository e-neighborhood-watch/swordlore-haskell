{-# Language TemplateHaskell #-}
module Types.Wood where

import Lens.Simple

data Wood = Wood
  { _woodName :: String
  }
  deriving
    ( Ord
    , Eq
    )
makeLenses ''Wood
