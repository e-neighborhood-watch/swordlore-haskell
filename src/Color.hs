module Color where

import Basics
import Control.Monad
import Control.Monad.Random

simpleColor ::
  ( MonadRandom m
  )
    => m String
simpleColor = choose 
  [ "red"
  , "black"
  , "brown"
  , "green"
  , "blue"
  , "white"
  , "orange"
  , "gray"
  , "purple"
  , "yellow"
  ]

complexColor ::
  ( MonadRandom m
  )
    => m String
complexColor = do
  color1 <- simpleColor
  color2 <- choose 
    [ "alabaster"
    , "amber"
    , "azure"
    , "beige"
    , "bronze"
    , "burgundy"
    , "carnelian"
    , "celadon"
    , "cerulean"
    , "crimson"
    , "ochre"
    , "gold"
    , "ivory"
    , "peach"
    , "saffron"
    , "scarlet"
    , "silver"
    , "vermillion"
    , "verdigris"
    ]
  choose [color1, color2]
  
