module Pokemove (pokemove) where

import System.Random
import Control.Monad

choose :: [a] -> IO a
choose x = do
  index <- randomRIO (0, length x - 1)
  return $ x !! index

chooseM :: [IO a] -> IO a
chooseM = join . choose

poketype :: IO String
poketype = chooseM
  [ choose 
    [ "Leaf"
    , "Grass"
    , "Petal"
    , "Vine"
    , "Flower"
    , "Wood"
    ]
  , return $ "Fairy"
  , choose
    [ "Moon"
    , "Lunar"
    ]
  , return $ "Bug"
  , choose
    [ "Poison"
    , "Venom"
    , "Toxic"
    , "Acid"
    , "Sludge"
    ]
  , choose
    [ "Electro"
    , "Storm"
    , "Thunder"
    ]
  , choose
    [ "Ice"
    , "Snow"
    ]
  , choose 
    [ "Fire"
    , "Flame"
    , "Blaze"
    , "Burn"
    , "Magma"
    , "Ember"
    ]
  , return $ "Psychic"
  , choose
    [ "Metal"
    , "Steel"
    , "Silver"
    , "Gold"
    , "Iron"
    ]
  , choose
    [ "Dragon"
    , "Meteor"
    , "Comet"
    ]
  , choose
    [ "Air"
    , "Wind"
    ]
  , choose
    [ "Rock"
    , "Stone"
    , "Sand"
    ]
  , choose
    [ "Bubble"
    , "Hydro"
    , "Water"
    , "Mist"
    , "Aqua"
    ]
  , choose
    [ "Shadow"
    , "Dark"
    , "Ghost"
    , "Vacuum"
    ]
  , return $ "Solar"
  , return $ "Aurora"
  , return $ "Aroma"
  , return $ "Bone"
  , return $ "Time"
  ]

bodyPart :: IO String
bodyPart = choose
  [ "Fang"
  , "Claw"
  , "Shell"
  , "Armor"
  , "Horn"
  , "Wing"
  , "Tail"
  , "Bone"
  ]

actionMod :: IO String
actionMod = chooseM
  [ poketype
  -- , bodyPart
  , choose
    [ "Focus" 
    , "Searing"
    , "Quick"
    , "Double"
    , "Triple"
    , "Super"
    , "Hyper"
    , "Mega"
    , "Giga"
    , "Jump"
    , "Dynamic"
    , "Bullet"
    , "Quiver"
    , "Sword"
    , "Sheild"
    , "Fury"
    , "Spinning"
    , "Gyro"
    , "Supersonic"
    ]
  ]

action :: IO String
action = choose
  [ "Crush"
  , "Smash"
  , "Slash"
  , "Mash"
  , "Kick"
  , "Punch"
  , "Jab"
  , "Dance"
  , "Thrust"
  , "Attack"
  , "Drain"
  , "Chop"
  , "Sting"
  ]

specialFormMod :: IO String
specialFormMod = chooseM
  [ poketype
  , choose
    [ "Energy"
    , "Hyper"
    , "Charge"
    , "Focus" 
    , "Searing"
    , "Mega"
    , "Giga"
    , "Bullet"
    , "Fury"
    , "Gyro"
    , "Photon"
    ]
  ]

specialForm :: IO String
specialForm = choose
  [ "Burst"
  , "Blast"
  , "Ball"
  , "Beam"
  , "Ripple"
  , "Wave"
  , "Bolt"
  , "Mist"
  , "Storm"
  , "Downpour"
  , "Pulse"
  , "Vortex"
  , "Missile"
  , "Tornado"
  , "Knot"
  , "Laser"
  ]

attackAdj :: IO String
attackAdj = chooseM
  [ poketype
  , chooseM
    [ return $ "Toxic"
    , choose
      [ "Sacred"
      , "Ancient"
      , "Cursed"
      , "Secret"
      , "Magic"
      ]
    , return $ "Razor"
    , return $ "Energy"
    , return $ "Strange"
    , return $ "Spirit"
    , choose
      [ "Nuclear"
      , "Fusion"
      , "Fission"
      ]
    , choose
      [ "Searing"
      , "Burning"
      , "Flaming"
      ]
    , choose
      [ "Icy"
      , "Frozen"
      , "Cold"
      , "Subzero"
      ]
    ]
  ]

attackObject :: IO String
attackObject = choose
  [ "Spikes"
  , "Bomb"
  , "Bullet"
  , "Powder"
  , "Spore"
  , "Sword"
  , "Blade"
  , "Leaf"
  , "Thread"
  , "Veil"
  , "Trap"
  , "Arrow"
  , "Hammer"
  , "Mirror"
  , "Shackle"
  ]

plotMod :: IO String
plotMod = chooseM
  [ choose
    [ "Nasty"
    , "Devious"
    , "Dastardly"
    , "Devilish"
    ]
  , choose
    [ "Clever"
    , "Cunning"
    , "Ingenious"
    ]
  , choose
    [ "Secret"
    , "Hidden"
    , "Concealed"
    , "Clandestine"
    ]
  ]

plot :: IO String
plot = choose
  [ "Plot"
  , "Plan"
  , "Trick"
  , "Trap"
  , "Surprise"
  ]

curseMod :: IO String
curseMod = choose
  [ "Ancient"
  , "Secret"
  , "Hidden"
  , "Grim"
  ]

curse :: IO String
curse = choose
  [ "Hex"
  , "Curse"
  , "Charm"
  , "Omen"
  , "Portent"
  , "Pact"
  , "Bond"
  , "Illusion"
  ]

(+++) = (++) . (++ " ")

pokemove :: IO String
pokemove = do
  chooseM
    [ liftM2 (+++) poketype bodyPart
    , liftM2 (+++) actionMod action
    , liftM2 (+++) specialFormMod specialForm
    , liftM2 (+++) attackAdj attackObject
    , liftM2 (+++) plotMod plot
    , liftM2 (+++) curseMod curse
    ]

