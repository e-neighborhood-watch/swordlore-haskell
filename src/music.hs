import Prelude hiding (until)

import System.Random
import Control.Monad

until :: (Monad m) => (a -> Bool) -> m a -> m a
until condition action = do
  res <- action
  if condition res
    then return res
    else until condition action

choose :: [a] -> IO a
choose x = do
  index <- randomRIO (0, length x - 1)
  return $ x !! index

chooseM :: [IO a] -> IO a
chooseM = join . choose

genre :: IO String
genre = do
  let
    microGenre = choose [
                          "step"
                        , "core"
                        , "wave"
                        ]
    microModifier = choose [
                             "punk"
                           , "synth"
                           , "hard"
                           , "soft"
                           , "speed"
                           , "math"
                           , "nerd"
                           , "vapor"
                           , "queer"
                           , "dub"
                           , "grime"
                           , "dark"
                           ]
    broadGenre = chooseM [
                           return "punk"
                         , return "metal"
                         , return "ska"
                         , return "rock"
                         , return "jazz"
                         , return "noise"
                         , return "folk"
                         , return "house"
                         , return "swing"
                         , return "rap"
                         , return "hip-hop"
                         , return "emo"
                         , return "techno"
                         , return "blues"
                         , return "disco"
                         , return "pop"
                         , return "funk"
                         , liftM2 (++) microModifier microGenre
                         ]
    toneType = choose [
                        "micro"
                      , "a"
                      , "mono"
                      , "hyper"
                      , "meta"
                      ]
  normalModifier <- chooseM [
                     return "art"
                   , return "sludge"
                   , return "witch"
                   , return "etherial"
                   , return "contemporary"
                   , return "absurdist"
                   , return "dream"
                   , return "future"
                   , return "symphonic"
                   , return "euro"
                   , return "grime"
                   , return "power"
                   , return "acid"
                   , return "incidental"
                   , return "experimental"
                   , return "industrial"
                   , return "ambient"
                   , fmap (++ "tonal") toneType 
                   , fmap (++ "tonal") toneType 
                   , return "death"
                   , return "electronic"
                   , return "hardcore"
                   , return "speed"
                   , return "smooth"
                   , return "lower-case"
                   , return "doom"
                   , return "existential"
                   , return "hard"
                   , return "black"
                   , return "christian"
                   , return "indie"
                   , return "garage"
                   , return "progressive"
                   , return "folk"
                   , return "mumble"
                   , return "lo-fi"
                   , return "classic"
                   , return "new wave"
                   , return "psychedelic"
                   , return "hair"
                   , return "glam"
                   , return "nu"
                   , return "skate"
                   , return "melodic"
                   , return "harsh"
                   , return "witch"
                   ]
  hyphenModifier <- choose [
                     "anarcho"
                   , "neo"
                   , "gangsta"
                   , "proto"
                   , "alt"
                   , "post"
                   , "electro"
                   , "anti"
                   ]

  chooseM [
            fmap ((normalModifier ++ " ") ++) broadGenre
--        , fmap ((hyphenModifier ++ "-") ++) broadGenre
          , do
             (a,b) <- until (uncurry (/=)) $ liftM2 (,) broadGenre broadGenre
             return $ a ++ " " ++ b
          ]
  
main :: IO ()
main = mapM_ (const $ genre >>= putStrLn) [1..10]
